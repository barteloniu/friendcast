import "./main.sass"
import io from "socket.io-client"

const socket = io()

socket.on("hello", () => {
	console.log("The server said hello")
})

console.log("Hello, world!")
