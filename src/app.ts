import * as path from "path"
import * as http from "http"
import express from "express"
import * as socketio from "socket.io"


const port = 3000
const app = express()
const server = http.createServer(app)
const io = require("socket.io")(server) as socketio.Server

app.use(express.static(path.resolve(__dirname, "../client/dist")))

io.on("connect", (socket: socketio.Socket) => {
	socket.emit("hello")
	console.log("A client has connected")
})

server.listen(port, () => {
	console.log(`Server listening on port ${port}`)
})
