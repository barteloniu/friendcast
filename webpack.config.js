const path = require("path")

module.exports = {
	entry: path.resolve(__dirname, "client/src/index.ts"),
	mode: "development",
	devtool: "inline-source-map",
	module: {
		rules: [
			{
				test: /\.ts$/,
				use: {
					loader: "ts-loader",
					options: {
						configFile: path.resolve(__dirname, "client/tsconfig.json")
					}
				},
				exclude: /node_modules/,
			},
			{
				test: /\.s[ac]ss$/,
				use: [
					"style-loader",
					"css-loader",
					"sass-loader"
				]
			}
		]
	},
	resolve: {
		extensions: [".ts", ".js"]
	},
	output: {
		filename: "bundle.js",
		path: path.resolve(__dirname, "client/dist")
	}
}
